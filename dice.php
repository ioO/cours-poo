<html>
<head>
  <title>Dice Roll</title>
</head>
<body>
<?php
Class Dice
{
  public $faces = array(1,2,3,4,5,6);

  function roll(){
    $key = array_rand($this->faces, 1);
    return $this->faces[$key];
  }
}

Class WeightedDice extends Dice
{
  function roll(){
    return $this->faces[5];
  }
}
$dice = new Dice();
$weighted_dice= new WeightedDice();
print("dice 1 : ".$dice->roll()."<br />");
print("dice 2 : ".$weighted_dice->roll());
?>
<br>
<button onclick='location.reload()'>Roll</button>
</body>
</html>
